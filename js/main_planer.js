function add() {
    if(document.getElementById('dod-input').value !== '')
    var li = document.createElement("li");
    var button = document.createElement('button');
    button.textContent = "Usuń";
    li.textContent = document.getElementById('dod-input').value;
    document.getElementById('zad').appendChild(li);
    li.appendChild(button);
    button.classList.add('usun-button');
    document.getElementById('dod-input').value = "";
}

document.addEventListener('click', function(event){
    if(event.target.classList.contains('usun-button')){
        document.getElementById('zad').removeChild(event.target.parentElement);
    }
})